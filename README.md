
# Add new Expensify reports to a Bexio account

Make sure your expense reports are available to your whole team, anytime. Use this automation to add new Expensify reports to your accounting. Your expense reporting will get an extra boost of visibility and everyone will stay in the know.

## How this integration works

Once the user has authentified into Expensify and Bexio through Zapier, she can set filters on Expensify reports, and decide which Bexio resource is the recipient for this integration.

Then, once this integration is running:

1. A new Expensify report is created
2. Zapier adds that report to the selected Bexio resource account

## Apps involved

Expensify
Bexio

