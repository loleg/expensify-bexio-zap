require('should');

const zapier = require('zapier-platform-core');

// Use this to make test calls into your app:
const App = require('../index');
const appTester = zapier.createAppTester(App);

const authExpensify = {
  partnerUserID: process.env.EXPENSIFY_ID,
  partnerUserSecret: process.env.EXPENSIFY_KEY
};

describe('expensify', () => {
  before(() => {
    if (!process.env.EXPENSIFY_ID || !process.env.EXPENSIFY_KEY) {
      throw new Error('For the tests to run, you need to do `export EXPENSIFY_ID=1234 EXPENSIFY_KEY=asdf`');
    }
  });

  it('connects to Expensify API', (done) => {
    appTester(App.expensify.test, authExpensify)
        .then((response) => {
          // response.status.should.eql(200);
          // response.request.url.should.containEql('partnerUserSecret');
          response.authConfirm.should.eql(true);
          done();
        })
        .catch(done);
  });

  it('gets a report from Expensify', (done) => {
    appTester(App.expensify.export, authExpensify)
        .then((response) => {
          // response.status.should.eql(200);
          // response.request.url.should.containEql('partnerUserSecret');
          response.should.eql(true);
          done();
        })
        .catch(done);
  });

});
