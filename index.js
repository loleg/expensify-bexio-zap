const expensify = require('./expensify');

// You'll want to set these with either `CLIENT_ID=abc zapier test` or `zapier env 1.0.0 CLIENT_ID abc`
process.env.BASE_URL = process.env.BASE_URL || 'http://zapier.webscript.io/platform-example-app';
process.env.CLIENT_ID = process.env.CLIENT_ID || '1234';
process.env.CLIENT_SECRET = process.env.CLIENT_SECRET || 'asdf';
process.env.EXPENSIFY_ID = process.env.EXPENSIFY_ID || null;
process.env.EXPENSIFY_KEY = process.env.EXPENSIFY_KEY || null;

const bexio = require('./bexio');

// To include the Authorization header on all outbound requests, simply define a function here.
// It runs runs before each request is sent out, allowing you to make tweaks to the request in a centralized spot
const includeBearerToken = (request, z, bundle) => {
  if (bundle.authData.access_token) {
    request.headers.Authorization = `Bearer ${bundle.authData.access_token}`;
  }
  return request;
};

// To include the API key header on all outbound requests, simply define a function here.
// It runs runs before each request is sent out, allowing you to make tweaks to the request in a centralized spot
// const includeApiKeyHeader = (request, z, bundle) => {
//   if (bundle.authData.apiKey) {
//     request.params = request.params || {};
//     request.params.api_key = bundle.authData.apiKey;
//   }
//   return request;
// };

// We can roll up all our behaviors in an App.
const App = {
  // This is just shorthand to reference the installed dependencies you have. Zapier will
  // need to know these before we can upload
  version: require('./package.json').version,
  platformVersion: require('zapier-platform-core').version,
  
  expensify: expensify,
  bexio: bexio,

  // beforeRequest & afterResponse are optional hooks into the provided HTTP client
  beforeRequest: [
    // includeApiKeyHeader
  ],

  afterResponse: [
  ],

  // If you want to define optional resources to simplify creation of triggers, searches, creates - do that here!
  resources: {
  },

  // If you want your trigger to show up, you better include it here!
  triggers: {
  },

  // If you want your searches to show up, you better include it here!
  searches: {
  },

  // If you want your creates to show up, you better include it here!
  creates: {
  }
};

// Finally, export the app.
module.exports = App;
