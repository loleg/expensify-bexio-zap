const testAuth = (z, bundle) => {
  const url = 'https://integrations.expensify.com/Integration-Server/ExpensifyIntegrations';
  const options = {
    method: 'POST',
    params: {
      'requestJobDescription' : JSON.stringify({
        type: 'download',
        credentials: {
            partnerUserID: bundle.partnerUserID,
            partnerUserSecret: bundle.partnerUserSecret
        }
      })
    }
  };
  return z.request(url, options).then((response) => {
      if (response.json.responseMessage === 'File not found') {
        response.authConfirm = true;
      } else {
        console.log (response.json);
        throw new Error('The API Key you supplied is invalid');
      }
      console.log("Authentication into Expensify platform verified.");
      return response;
    });
};

var fetch = require('node-fetch');
var FormData = require('form-data');
var fs = require('fs');

const testExport = (z, bundle) => {
  const url = 'https://integrations.expensify.com/Integration-Server/ExpensifyIntegrations';

  var options = {
    method: 'POST',
    headers: {},
    timeout: 30000
  };
  var params = {
      type: 'file',
      credentials: {
          partnerUserID: bundle.partnerUserID,
          partnerUserSecret: bundle.partnerUserSecret
      },
      onReceive: {
          immediateResponse: ["returnRandomFileName"]
      },
      inputSettings: {
          type: "combinedReportData",
          filters: {
              // reportIDList: "1234567,2233445"
              startDate: "2017-01-01"
          }
      },
      outputSettings: {
          fileExtension: "csv"
      }
  };

  function getFormLength(form) {
    return new Promise(function(resolve, reject) {
      form.getLength(function(err, length) {
        if (err !== null) return reject(err);
        resolve(length);
      });
    });
  }

  var form = new FormData();
  form.append('requestJobDescription', JSON.stringify(params));
  form.append('template',
'<#list reports as report>\
ID: ${report.reportID}, NAME: ${report.reportName}<#lt>\
</#list>');

  // Read template from file:
  //fs.createReadStream('expensify_template.ftl'));

  return getFormLength(form).then((length) => {
    options.body = form;
    options.headers['content-length'] = length;
    console.log("Obtaining list of expense reports")
    // console.log (options);
    return fetch(url, options)
      .then(res => res.text())
      .then(text => {
        try {
          console.log(JSON.parse(text));
          return false;
        } catch (e) {
          console.log('Processing ' + text);
          const csvFilename = text;
          var params2 = {
              type: 'download',
              credentials: {
                  partnerUserID: bundle.partnerUserID,
                  partnerUserSecret: bundle.partnerUserSecret
              },
              fileName: csvFilename
          };
          var form2 = new FormData();
          form2.append('requestJobDescription', JSON.stringify(params2));
          var options2 = {
            method: 'POST',
            body: form2
          };
          console.log("Downloading report data ...")
          return fetch(url, options2)
            .then(res => res.text())
            .then(text => {
              console.log("Data preview: ---")
              console.log(text);
              console.log("---")
              return true;
            })
        }
      });
  }); // -getLength
};

module.exports = {
  type: 'custom',
  // Define any auth fields your app requires here. The user will be prompted to enter this info when
  // they connect their account.
  fields: [
    {key: 'partnerUserID', label: 'Partner User ID', required: true, type: 'string'},
    {key: 'partnerUserSecret', label: 'Partner User Secret', required: true, type: 'string'}
  ],
  // The test method allows Zapier to verify that the credentials a user provides are valid. We'll execute this
  // method whenver a user connects their account for the first time.
  test: testAuth,
  export: testExport
};
